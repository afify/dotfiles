#!/usr/bin/env bash

export EDITOR=vim
export VISUAL=vim
export GPG_TTY=$(tty)
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_so=$'\e[01;44;37m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_ue=$'\e[0m'

export NNN_BMS='h:~/;d:/mnt/data/dev/;D:~/downloads/'
export NNN_USE_EDITOR=1
export NNN_SSHFS_OPTS="sshfs -o follow_symlinks"
export NNN_CONTEXT_COLORS="2136"
export NNN_TRASH=1
export NNN_OPENER=~/.config/nnn/.nnn_opener
export NNN_SHOW_HIDDEN=1

export HISTSIZE=10000
export HISTFILESIZE=10000
export HISTTIMEFORMAT='%b %d %I:%M %p '
export HISTCONTROL=ignoreboth
export HISTIGNORE="history:pwd:exit:df:ls:ls -lahG:ll"
export HISTFILE=~/.cache/.bash_history

export GREP_COLOR="30;47"
export _JAVA_AWT_WM_NONREPARENTING=1
