#!/usr/bin/env bash

stty -ixon # Disable ctrl-s and ctrl-q.
set -o vi  # vi mode

alias k='clear'
alias mv='mv -i'
alias cp='cp -ir'
alias rm='rm -i'
alias mkdir='mkdir -p'
alias xc='xclip -sel clip'
alias f='~/.scripts/find.sh'
alias g='~/.scripts/gpg.sh'
alias fw='~/.scripts/find_word.sh'
alias fr='~/.scripts/find_replace_word.sh'
alias fonts="fc-list | sed 's/^.*: //' | sort"

if [[ $(uname) == "OpenBSD" ]]; then
	alias ll='colorls -lahG'
	alias lsb="dmesg | egrep '^(cd|wd|sd|fd). at '"
	alias ka="pkill"
	alias wifiq='bash .scripts/openbsd_connect_wifi.sh'
	alias wifin='doas vim /etc/hostname.athn0'
	alias startn='doas sh /etc/netstart'
fi

if [[ $(uname) == "Linux" ]]; then
	alias df='df -h'
	alias ka='killall'
	alias ll='ls -lahG --color=auto --group-directories-first'
	alias pm="sudo pacman"
	alias pmu="sudo pacman -Syyu"
	alias pmo="sudo pacman -Rns $(pacman -Qtdq)"
	alias pmr="sudo pacman -Rns"
	alias aur="yaourt -Qm --aur"
	alias grep="grep --color=always"
fi

alias dwmrc="cd /mnt/data/dev/built/dwm-6.2/ && vim config.h"
alias strc="cd /mnt/data/dev/built/st-0.8.2/ && vim config.h"
alias sl="cd /mnt/data/dev/built/slstatus/ && vim config.h"

alias ya="youtube-dl -i --add-metadata -f bestaudio"
alias yv="youtube-dl -i --add-metadata -f webm"
alias yvp="youtube-dl -i --add-metadata -f webm -o \"%(autonumber)s-%(title)s.%(ext)s\""

#------ PROMPT ----------------------------------------------------------------
esc="\[\033"
export PS1="$esc[38;5;214m\]$esc[48;5;0m\] \W $esc[48;5;233m\]$esc[38;5;166m\]\$(__git_ps1 ' %s ')$esc[00m\] "

#------ Archive extractor -----------------------------------------------------
ex (){
if [ -f "$1" ] ; then
	case $1 in
		*.tar.bz2)   tar xjf "$1"    ;;
		*.tar.gz)    tar xzf "$1"    ;;
		*.tar.xz)    tar xf  "$1"    ;;
		*.xz)        xz -d   "$1"    ;;
		*.bz2)       bunzip2 "$1"    ;;
		*.rar)       unrar x "$1"    ;;
		*.gz)        gunzip  "$1"    ;;
		*.tar)       tar xf  "$1"    ;;
		*.tbz2)      tar xjf "$1"    ;;
		*.tgz)       tar xzf "$1"    ;;
		*.zip)       unzip   "$1"    ;;
		*.Z)         uncompress "$1" ;;
		*.7z)        7z x "$1"       ;;
		*)           echo "'$1' cannot be extracted via ex()";;
	esac
else
	echo "'$1' is not a valid file"
fi
}

#------ GIT -------------------------------------------------------------------
if [ -f ~/.git-completion.bash ]; then
	source ~/.git-completion.bash
fi

if [ -f ~/.git-prompt.sh ]; then
	source ~/.git-prompt.sh
fi
