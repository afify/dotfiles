#!/usr/bin/env bash

#==============================================================================
# Name        : bluetooth_call
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

# action=$(echo -e "call\nrestore" | dmenu -p "Activate bluetooth Mic" )
# pactl list | grep -A2 'Source #' | grep 'Name: .*\.monitor$' | cut -d" " -f2

device=FC_A8_9A_57_A4_8A
mac_device=FC:A8:9A:57:A4:8A

profile_is_a2dp=$(pactl list |\
	grep -A2 'Source #' |\
	grep 'Name: ' |\
	cut -d" " -f2 |\
	grep a2dp)

change_to_head_unit(){
	# convert profile to headset_head_unit
	if ! set_headset=$(pacmd set-card-profile bluez_card.$device headset_head_unit); then
		return 1
	fi
	# unmute mic
	pacmd set-source-mute bluez_source.$device.headset_head_unit 0
	pacmd set-source-mute bluez_sink.$device.headset_head_unit.monitor 1
	# set volume
	pacmd set-source-volume bluez_source.$device.headset_head_unit 40000
}


if [ $profile_is_a2dp ]; then
	# pause any music
	playerctl pause
	if ! change_to_head_unit; then
		echo "failed"
		bluetoothctl disconnect $mac_device
		bluetoothctl connect $mac_device
		change_to_head_unit
	fi
	notify-send -u critical "MIC" "ON"

else
# 	playerctl play
	notify-send "MIC" "OFF"
	# restore to default
	pacmd set-source-mute bluez_source.$device.headset_head_unit 1
	pacmd set-card-profile bluez_card.$device a2dp_sink
	# TODO
	# if failed restart bluetooth
	pacmd set-source-mute bluez_sink.$device.a2dp_sink.monitor 1

	# Move all apps to bluetooth device
	inputs=$(pacmd list-sink-inputs | awk '$1 == "index:" {print $2}')
	for apps in $inputs; do
		pacmd move-sink-input $apps bluez_sink.$device.a2dp_sink
	done
fi
