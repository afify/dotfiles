#!/usr/bin/env bash

#==============================================================================
# Name        : mount_drives
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description : Mount drives using dmenu
# Depencency  : simple-mtpfs, cryptsetup
#
# sudo  dmsetup ls (low level logical volume managment)
# sudo cryptsetup status /dev/mapper/volume
#==============================================================================

if [[ $(uname) == "OpenBSD" ]]; then
	all_disks=$(sysctl -n hw.disknames | grep -o '[cwsf]d[0-9]')
	mounted=$(df | awk '{print $1}' | grep -o '[cwsf]d[0-9]' | uniq |\
				tr '\n' '|'| sed -E 's/\|$//g')
	unmounted=$(echo "$all_disks" | sed -E "s/($mounted)//g")
# 					tr '\n' '|' | grep -o '[cwsf]d[0-9]|' |\
# 					tr -d '\n' | sed -E 's/\|$//g'

# simple-mtpfs --device 1 /mnt/mtp -o uid=1000 -o gid=1000 -o allow_other

	unmounted_description=$(
	for disk in $unmounted; do
		dmesg | grep "^$disk" | uniq |\
		tr -d '\n' |\
		sed -E "s/at.*<|$disk: / /g" |\
		sed -E "s/MB.*/MB\\\n/g"
	done
	)
# 	unmounted_description=$(
# 		dmesg | egrep "($unmounted)" | uniq |\
# 		tr -d '\n' |\
# 		sed -E "s/ at.*:|($unmounted): //g" |\
# 		sed -E "s/MB.*/MB\\\n/g" 
# 	)

	partition=$(echo -e "$unmounted_description" |\
					dmenu -l 10 | awk '{print $1"i"}')
	dev=dev

elif [[ $(uname) == "Linux" ]]; then
	# TODO
	# mounted encrypted volume is still in mount options (crypto_LUKS)
	# do not print android if mounted
	# 	awk '$1~/[[:digit:]]/ && $3~/[[:alnum:]]/ && $4 == "";\
	# findmnt show mounted
	chosen=$(\
		lsblk --noheadings --raw -o NAME,SIZE,FSTYPE,TYPE,MOUNTPOINT |\
		awk '$4 == "part" && $5 == "";\
			END {system("simple-mtpfs --list-devices")}'|\
		dmenu -i -p "Mount volume" -l 10 )

	partition=$(echo $chosen | awk '{print $1}')
	file_type=$(echo $chosen | awk '{print $3}')
	android_d=$(echo $partition | awk '/^[0-9]/' | head -c 1)

	# android
		# TODO
		# mount android even locked screen
	if [ $android_d ]; then
		mount_point=$(find /mnt/ -maxdepth 1 -type d -empty -name android* |\
			dmenu -i -p "Select mount point")
		simple-mtpfs --device $android_d $mount_point\
			&& notify-send "Mounted" "Android"\
			|| notify-send -u critical "Error Mounting Android" "Unlock screen"
		exit 0
	fi

	# unlock encrypted partition
		# FIXME:
		# mount luks non root mount_point
	if [ "$file_type" == "crypto_LUKS" ]; then
		dev=dev/mapper

		# check if locked
		if [ ! -b /dev/mapper/$partition ]; then
			if dmenu -P -p 'cryptsetup | sudo '|\
				sudo -S sh -c "dmenu -P -p 'luks volume |  password ' |\
				cryptsetup luksOpen /dev/$partition $partition"; then
				notify-send "Unlocked" "$partition"
			else
				notify-send -u critical "Error Unlocking $partition"
				exit 0
			fi
		fi

	else # file_type != crypto_LUKS
		dev=dev
	fi

fi # end of OS detection

# Mount partition
if [ $partition ]; then
	mount_point=$(find /mnt/ -maxdepth 1 -type d -empty | sort |\
		dmenu -i -p "Select mount point")
	if [ $mount_point ]; then
		dmenu -P -p "Mount | sudo " |\
		sudo -S mount /$dev/$partition $mount_point\
			&& notify-send "Mounted" "$partition"\
			|| notify-send -u critical "Error Mounting" "$partition"
	fi
fi
