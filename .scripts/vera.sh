#!/usr/bin/env bash

#==============================================================================
# Name        : veracrypt
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.2
# Description : Create encrypted volume file
#==============================================================================

# TODO
# generate a year files

distination="/home/hassan"
filename="$distination/secret_file.vera"
password="topsecret"
size="10m"

encryption="AES"
filetype="exfat"
hash_type="sha-512"
volume_type="normal"

#Create a new volume:
veracrypt -t --create $filename --filesystem $filetype --encryption $encryption --password $password --size $size --volume-type $volume_type --hash $hash_type --pim=0 -k "" --random-source=/dev/urandom


#Mount a volume:
#===============
#veracrypt --mount filename /media/vera/

#Unmout all volumes:
#===================
#veracrypt -d
