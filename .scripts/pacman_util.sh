#!/usr/bin/env sh

######################################################################
# @author      : hassan (hassan@alien)
# @file        : pacman_util
# @created     : Wednesday Oct 09, 2019 20:37:41 +03
#
# @description : 
######################################################################

cache packages
sudo ls /var/cache/pacman/pkg/ | wc -l

disk usage
du -sh /var/cache/pacman/pkg/

remove uninstalled pachages
sudo pacman -Sc

clean all cache
sudo pacman -Scc

pacman -Qe | awk '{print $1}' > installed.txt
