#!/usr/bin/env bash

Add your user to the staff group. This group has higher resource limits in login.conf. You'll need to log out and back in for this change to take effect.

usermod -G staff YOUR_USERNAME
The default resource limits in OpenBSD are extremely conservative. For running modern applications like web browsers, we'll need to bump them up significantly. Use vi to modify the staff login class in /etc/login.conf as follows.

/etc/login.conf
staff:\
  :datasize-cur=1024M:\
  :datasize-max=8192M:\
  :maxproc-cur=512:\
  :maxproc-max=1024:\
  :openfiles-cur=4096:\
  :openfiles-max=8192:\
  :stacksize-cur=32M:\
  :ignorenologin:\
  :requirehome@:\
  :tc=default:
