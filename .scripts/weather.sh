#!/usr/bin/env bash

#==============================================================================
# Name        : weather
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

city=Makkah

curl wttr.in/$city?format=1
