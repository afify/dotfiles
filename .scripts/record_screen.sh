#!/usr/bin/env bash

save_at="$HOME/captured"
time_date=$(date +'%Y_%m_%d_%H_%M_%S')
# monitor_size="1920x1080"
monitor_size="1056x1080"
frate="25"
encoding="libx264 -preset ultrafast"
# encoding="ffvhuff"
# hide_mouse="-draw_mouse 0"

is_running=$(pgrep ffmpeg | wc -l )
if [[ $is_running -eq 0 ]]; then
	[ -d $save_at ] || mkdir $save_at
	ffmpeg -f x11grab -video_size $monitor_size -framerate $frate $hide_mouse -i $DISPLAY -c:v $encoding $save_at/$time_date.mkv
else
	pkill ffmpeg
fi
