#!/usr/bin/env bash

#==============================================================================
# Name        : find
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
# Notes       :
#               -size +1000MB
#               -type f
#               -empty
#               -mtime 7 (modified 7 days)
#               -user
#               -maxdepth X
#               -not (not match)
#               -delete (delete match)
#               f – regular file
#               d – directory
#               l – symbolic link
#               c – character devices
#               b – block devices
#==============================================================================

dir=$1
file=$2
# find / -type f -iname "filename"

find $dir -type f -iname "*$file*"
