#!/usr/bin/env bash

force(){
	pulseaudio -k
	dmenu -P -p "sudo " | sudo -S sh -c\
		"rfkill unblock bluetooth
		systemctl stop bluetooth
		systemctl start bluetooth"
	}

check_bluetooth(){
	bstatus=$(systemctl is-active bluetooth)
	bl_s=$(rfkill -rn | awk '/bluetooth/ {print $4}')
	if [[ "$bstatus" != "active"  || "$bl_s" != "unblocked" ]]; then
		force
	fi
}

select_device(){
	device=$(echo -e "JBL Headset\nAnne Pro" | dmenu -i -p " ")
	if [ "$device" == "Anne Pro" ]; then
		device='4C:24:98:32:B7:01'
		device_name="Anne Pro"
	elif [ "$device" == "JBL Headset" ]; then
		device='FC:A8:9A:57:A4:8A'
		device_sink='FC_A8_9A_57_A4_8A'
		device_name="JBL Headset"
	else exit
	fi
}

connect_device(){
	is_connected=$(bluetoothctl info $device | awk '/Connected/ {print $2}')
	if [[ ! "$is_connected" == "yes" ]]; then
		bluetoothctl power off
		bluetoothctl power on
		bluetoothctl disconnect $device
		bluetoothctl pair $device
		bluetoothctl connect $device
		bluetoothctl trust $device
	fi
}

jbl(){
	if [[ "$device_name" == "JBL Headset" ]]; then
		change=$(pacmd set-card-profile bluez_card.$device_sink a2dp_sink |\
			grep -o "Failed")
		if [[ "$change" == "Failed" ]]; then
			force
			jbl
		else
			pacmd set-default-sink bluez_sink.$device_sink.a2dp_sink
			inputs=$(pacmd list-sink-inputs | awk '$1 == "index:" {print $2}')
			for apps in $inputs; do
				pacmd move-sink-input $apps bluez_sink.$device_sink.a2dp_sink
			done
		fi
	fi
}

pulseaudio_fix(){
	pulseaudio -k
}

check_bluetooth
select_device
connect_device
jbl
