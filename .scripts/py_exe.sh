#!/usr/bin/env bash
# This script encode a python script with base64 and generate exe file

random_string1=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 128 ; echo '')
random_string2=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 128 ; echo '')
random_string3=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12 ; echo '')

icon_path='final_wolf_tzh_icon.ico'
version_file='spec.txt'

python_src_without_extention='hello_world'
python_src_file=$python_src_without_extention.py
python_src_file_content=$(cat $python_src_file | base64)

decoder_template="import base64,sys;exec(base64.b64decode({2:str,3:lambda b:bytes(b,'UTF-8')}[sys.version_info[0]]('$python_src_file_content')))"

tmp_file_name=/tmp/$random_string3.py
final_file=$python_src_without_extention.exe

# echo $python_src_file_content
# echo $decoder_template
# echo 
# python -c "$decoder_template"
echo "#$random_string1
$decoder_template
#$random_string2
" > $tmp_file_name

pyinstaller --onefile --nowindowed --noconsole --icon=$icon_path --version-file=$version_file $tmp_file_name
