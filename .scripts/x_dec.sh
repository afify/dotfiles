#!/usr/bin/env bash

mon_status=$(xbacklight -get | bc )
mon_status=${mon_status%.*}

if [[ "mon_status" -eq "0" ]];
	then sleep 0.1 && xset dpms force off
else xbacklight -dec 10;fi
