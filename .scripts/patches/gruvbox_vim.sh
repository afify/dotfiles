#!/usr/bin/env bash

# patch gruvbox
patch_dir="/home/hassan/.scripts/patches"
gruv_default="$HOME/.vim/plugged/gruvbox/colors/gruvbox.vim"
mydiff="$patch_dir/gruvbox.diff"

patch $gruv_default <  $mydiff
