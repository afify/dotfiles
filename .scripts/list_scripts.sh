#!/usr/bin/env bash

#==============================================================================
# Name        : my_scripts
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

scripts_dir=/home/hassan/.scripts

if choosen_script=$(\
	find $scripts_dir  -maxdepth 1 -type f -printf "%f\n" |\
	sort |\
	dmenu -l 10 -i -p "run script"); then

sh $scripts_dir/$choosen_script

fi
