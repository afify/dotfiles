#!/usr/bin/env bash

interface=athn0
ssid=$1
pass=$2

doas -- bash -c " ifconfig $interface up ; ifconfig $interface nwid $ssid wpakey $pass ; dhclient $interface"
