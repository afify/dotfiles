#!/usr/bin/env bash

#==============================================================================
# Name        : lock
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

slock & sleep 1 && xset dpms force off
