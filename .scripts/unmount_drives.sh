#!/usr/bin/env bash

#==============================================================================
# Name        : unount_drives
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description : Unmount drives using dmenu.
# Notes       :
#                fusermount -u <mountpoint>>
#                udisksctl mount -b /dev/sdb1
#                udisksctl unmount -b  /dev/sdd1
#                udisksctl power-off -b  /dev/sdd1
# $chosen:
# - awk first variable must contain digit (dont print /dev/sda )
# - and forth variable contains /mnt (means is mounted at /mnt)
# - for android case we find in /mnt for android* not empty directories
#      this is not perfect solution to find mounted android devices.
# - if nothing is chosen exit 0

#==============================================================================


if [[ $(uname) == "OpenBSD" ]]; then

	mounted=$(df | awk '{print $1" "$6}' | egrep -o '([cwsf]d[0-9]. /mnt.*)')
	mount_point=$(echo "$mounted" | dmenu -l 10 | awk '{print $2}')

	if [ $mount_point ]; then
		dmenu -P -p "umount $mount_point | sudo " | sudo -S umount $mount_point\
			&& notify-send "Unmounted" "$mount_point"\
			|| notify-send -u critical "Error Unmounting" "$mount_point"
	fi


elif [[ $(uname) == "Linux" ]]; then

chosen=$(\
	lsblk --noheadings --raw -o NAME,SIZE,TYPE,MOUNTPOINT |\
	awk '$1~/[[:digit:]]/ && $4~/\/mnt/;\
		END {system("find /mnt/ -maxdepth 1 -type d\
		-not -empty -name android*")}'|\
	dmenu -i -p "Unmount volume" -l 10 )

partion=$(echo $chosen | awk '{print $1}')      # sda#
file_type=$(echo $chosen | awk '{print $3}')    # crypt | part
mount_point=$(echo $chosen | awk '{print $4}')  # /mnt/usb1
android_d=$(echo $chosen | awk '/android/')     # /mnt/.android

# android
if [ $android_d ]; then
	fusermount -u $android_d\
		&& notify-send "Unmounted" "$android_d"\
		|| notify-send -u critical "Error Unmounting" "$android_d"
	exit 0
fi

# Unmount partion
if [ $mount_point ]; then
	dmenu -P -p "umount $mount_point | sudo " | sudo -S umount $mount_point\
		&& notify-send "Unmounted" "$mount_point"\
		|| notify-send -u critical "Error Unmounting" "$mount_point"
		# fuser -vm /mnt/dir show ps
		# fuser -k /mnt/dir  kill
		# umount -l /dev/sdX lazy
fi

# If the drive is encrypted lock after umount
if [ "$file_type" == "crypt" ]; then
	dmenu -P -p "luksClose | sudo " |\
	sudo -S cryptsetup luksClose /dev/mapper/$partion\
		&& notify-send "Locked" "$partion"\
		|| notify-send -u critical "Error Locking" "$partion"
fi
fi
