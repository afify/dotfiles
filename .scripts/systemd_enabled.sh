#!/usr/bin/env sh

######################################################################
# @author      : hassan (hassan@archlinux)
# @file        : systemd_enabled
# @created     : Wednesday Oct 09, 2019 09:41:47 +03
#
# @description : 
######################################################################

systemctl list-unit-files --state=enabled


