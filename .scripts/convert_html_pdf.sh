#!/usr/bin/env bash

#==============================================================================
# Name        : convert_html_pdf
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

html_file=$1
output_pdf=$html_file.pdf
page_size='A4'

wkhtmltopdf -s $page_size $html_file $output_pdf
