#!/usr/bin/env bash

step=10
p_step=5

if [[ $(uname) == "OpenBSD" ]]; then
	master=$(mixerctl outputs.master | egrep -o '([0-9]{1,3})')
	left_master=$(echo $master | awk '{print $1}')
	right_master=$(echo $master | awk '{print $2}')
	mute_status=$(mixerctl outputs.master.mute | egrep -o '(on|off)')
	headset_status=$(mixerctl outputs.hp_sense | egrep -o '(plugged|unplugged)')
	mute_headset_status=$(mixerctl outputs.hp_mute | egrep -o '(on|off)')
fi

	inc_vol(){
	case $(uname) in
		Linux) pactl set-sink-volume 0 +$p_step% ;;
		OpenBSD)
			if [[ "$headset_status" == "plugged" ]]; then
				left=$(($left_master + $step))
				right=$(($left - 20))
				mixerctl outputs.master="$left,$right"
			else
				mixerctl outputs.master=$(($right_master + $step))
			fi;;
	esac
	}

	dec_vol(){
	case $(uname) in
		Linux)  pactl set-sink-volume 0 -$p_step% ;;
		OpenBSD)
			if [[ "$headset_status" == "plugged" ]]; then
				left=$(($left_master - $step))
				right=$(($left - 20))
				mixerctl outputs.master="$left,$right"
			else
				mixerctl outputs.master=$(($right_master - $step))
			fi;;
	esac
	}

	mute(){
	case $(uname) in
		Linux) pactl set-sink-mute 0 toggle ;;
		OpenBSD)
		if [[ "$headset_status" == "plugged" ]]; then
			if [[ "$mute_headset_status" == "off" ]];
				then mixerctl outputs.hp_mute=on;
				else mixerctl outputs.hp_mute=off;
			fi

		else
			if [[ "$mute_status" == "off" ]];
				then mixerctl outputs.master.mute=on;
				else mixerctl outputs.master.mute=off;
			fi
		fi;;
	esac
	}

	mic_status(){
	case $(uname) in
		Linux)
			unmuted_count=$(pactl list sources| grep 'Mute: no' | wc -l)
			if [[ $unmuted_count -gt 0 ]];
			then echo " $unmuted_count 壘 ";fi ;;
		OpenBSD)
			kern_status=$(sysctl kern.audio.record | grep -o [0-9])
			case $kern_status in
				0) exit          ;;
				1) echo " 壘 " ;;
			esac
	esac
	}

	case $1 in
		inc)    inc_vol    ;;
		dec)    dec_vol    ;;
		mute_t) mute       ;;
		mic)    mic_status ;;
	esac
