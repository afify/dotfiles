#!/usr/bin/env bash

count_login=$(w -h | grep -cv xinit)
if [[ $count_login -gt 0 ]]; then
	result+=" $count_login 﫻 "
fi

count_logout=$(pgrep -a ssh | grep -c "ssh ")
if [[ $count_logout -gt 0 ]]; then
	result+=" $count_logout  "
fi

sshd=$(systemctl is-active sshd)
if [[ "$sshd" == "active" ]]; then
	result+=" [sshd]"
fi

echo $result
