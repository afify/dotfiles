#!/usr/bin/env bash

distro=$(uname -a | awk '{print $2}')

case $OSTYPE in
	"linux-gnu") echo linux $distro ;;
	"darwin"*)   echo mac           ;;
	"openbsd"*)  echo openbsd       ;;
esac
