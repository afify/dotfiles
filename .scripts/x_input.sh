#!/usr/bin/env bash

xdir="/usr/share/X11/xorg.conf.d"

# arabic and touchpad
echo "# Match on all types of devices but joysticks
Section \"InputClass\"
	Identifier \"libinput pointer catchall\"
	MatchIsPointer \"on\"
	MatchDevicePath \"/dev/input/event*\"
	Driver \"libinput\"
EndSection

Section \"InputClass\"
	Identifier \"libinput keyboard catchall\"
	MatchIsKeyboard \"on\"
	MatchDevicePath \"/dev/input/event*\"
	option \"XkbLayout\" \"us, ar\"
	option \"XkbOptions\" \"grp:win_space_toggle\"
	Driver \"libinput\"
EndSection

Section \"InputClass\"
	Identifier \"libinput touchpad catchall\"
	MatchIsTouchpad \"on\"
	MatchDevicePath \"/dev/input/event*\"
	Option \"NaturalScrolling\" \"on\"
	Option \"ScrollMethod\" \"two-finger\"
	Option \"Tapping\" \"on\"
	Driver \"libinput\"
EndSection

Section \"InputClass\"
	Identifier \"libinput touchscreen catchall\"
	MatchIsTouchscreen \"on\"
	MatchDevicePath \"/dev/input/event*\"
	Driver \"libinput\"
EndSection

Section \"InputClass\"
	Identifier \"libinput tablet catchall\"
	MatchIsTablet \"on\"
	MatchDevicePath \"/dev/input/event*\"
	Driver \"libinput\"
EndSection" > $xdir/40-libinput.conf
