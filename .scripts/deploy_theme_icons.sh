#!/usr/bin/env bash

confdir='/mnt/data/resources'
dist="$HOME/.local/share"

if [[ ! -d "$dist/themes" ]]; then
	mkdir -p $dist/themes
	cp -R $confdir/Mojave-dark $dist/themes/
fi

if [[ ! -d "$dist/icons" ]]; then
	mkdir -p $dist/icons
	cp -R $confdir/la-capitaine-icon-theme $dist/icons/
fi


if [[ ! -f "$HOME/.gtkrc-2.0" ]]; then
	echo "gtk-icon-theme-name = Mojave-dark
	gtk-theme-name = la-capitaine-icon-theme
	#gtk-font-name = Terminus 12" > '~/.gtkrc-2.0'
fi

if [[ ! -f "$HOME/.config/gtk-3.0/settings.ini" ]]; then
	mkdir -p '.config/gtk-3.0'
	echo "[Settings]
	gtk-application-prefer-dark-theme = true
	gtk-theme-name = Mojave-dark
	gtk-icon-theme-name = la-capitaine-icon-theme
	#gtk-font-name = Monospace 11" > '~/.config/gtk-3.0/settings.ini'
fi
