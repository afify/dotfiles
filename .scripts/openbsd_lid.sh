#!/usr/bin/env bash

Since this is a laptop, you'll probably want your screen to lock automatically when you close the lid. You can configure apmd to do this for you. (Note that this won't be effective until we configure X11 in the section below.)

First, make the directory:

mkdir /etc/apm
Then, create the file /etc/apm/suspend with the following contents:

/etc/apm/suspend
#!/bin/sh
pkill -USR1 xidle
And make it executable:

chmod +x /etc/apm/suspend
I'm paranoid and don't like the fact that ntpd reaches out to www.google.com of all places to sanity check each clock update. You can turn that off easily:

sed -i '/google/d' /etc/ntpd.conf
Or, even better, you can replace Google with domain you feel comfortable pinging all the time.

sed -i 's/www\.google\.com/www.example.com/' /etc/ntpd.conf
If you do either of those, be sure to restart ntpd:

rcctl restart ntpd
Now you've got a pretty robust OpenBSD installation. You could probably take over the world with just the base system, but we'll take some time to make OpenBSD into a more pleasant desktop experience. Exit from your root shell, and we'll start configuring X11.
