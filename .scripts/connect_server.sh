#!/usr/bin/env bash

ssh_config=$HOME/.ssh/config
known_hosts=$(awk '/Host / && !(/github/ || /gitlab/ || /\*/) {print $2}'\
	$ssh_config)

choose=$(echo -e "$known_hosts\n- scan\n- manual" | dmenu -l 10 -p 'Choose: ')

case $choose in
	"- scan") server_ip=$(bash $HOME/.scripts/scan_network.sh ssh|\
		dmenu -l 10 -p 'Select ip: ') ;;
	"- manual") server_ip=$(echo '' | dmenu -l 10 -p 'Server ip: ' |\
		tr -d ' ') ;;
	*) server_ip=$choose ;;
esac

if [[ $server_ip ]]; then
	server_ip=$(echo $server_ip | awk '{print $1}')
	added_host=$(grep "$server_ip" "$HOME/.ssh/config")
	con_type=$(echo -e "ssh\nsshfs" | dmenu -l 10 -p 'Con type: ')
else
	exit
fi

if [[ $added_host ]]; then
	user=$(awk "/$server_ip/,/User/" "$HOME/.ssh/config" |\
		awk '/User/ {print $2"@"}')
	key=$(awk "/$server_ip/,/IdentityFile/" "$HOME/.ssh/config" |\
		awk '/IdentityFile/ {print $2}')
else
	user=$(echo ' ' | dmenu -l 10 -p 'Type user: ' | tr -d ' ')'@'
	key=$(bash $HOME/.scripts/ssh public)
fi

case $con_type in
	ssh) st ssh $user$server_ip ;;
	sshfs)
		mount_point=$(find /mnt/ -maxdepth 1 -type d -empty | sort |\
		dmenu -i -p "Select mount point")
		if [ $mount_point ]; then
			dmenu -P -p "Mount | sudo " |\
				sudo -S sshfs -o allow_other \
					-o IdentityFile=$key $user$server_ip:. $mount_point\
				&& notify-send "Mounted" "$server_ip"\
				|| notify-send -u critical "Error Mounting" "$server_ip"
		fi ;;
esac

# TODO
# mount -t fuse.sshfs
# sshfs_info
