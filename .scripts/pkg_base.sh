git
hub

password-store

sudo
bash
# grc
# gtk2
# gtk3

# nvidia-driver
# nvidia-settings

dunst
htop
# fd
jq

# tcc
splint
valgrind
# hyperfine
python
# py36-pip
# pkgconf

nmap
whois

feh
uget
sxiv
slock
neomutt
calcurse
newsboat
# veracrypt
# webkit2-gtk3
wkhtmltopdf
libreoffice
# transmission-gtk

mpv
vlc
# youtube_dl

vi
vim
vifm
mupdf

gimp
inkscape

tor
unrar
unzip

# fusefs-ext4fuse

hashcat
# virtualbox

# nerd-fonts
terminus-font
