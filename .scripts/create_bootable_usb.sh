#!/usr/bin/env bash

#==============================================================================
# Name        : create_bootable_usb
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

usb=$(\
	lsblk --noheadings --raw -o NAME,SIZE,MOUNTPOINT |\
# 	awk '$1~/[[:digit:]]/ && $3~/\/mnt/' |\
	awk '$1~/[[:digit:]]/' |\
	dmenu -i -p "Choose disk"|\
	awk '{print $1'})

if [ $usb ]; then
	target=/mnt/4tb/os/
	[ -z "$target" ] && target="$(realpath .)"
	prompt="$2"

	while true; do
		p="$prompt"
		[ -z "$p" ] && p="$target"
		sel="$(ls -1a "$target" | grep -v '^\.$' | dmenu -p "$p" -l 25)"
		ec=$?
		[ "$ec" -ne 0 ] && exit $ec

		c="$(echo "$sel" | cut -b1)"
		if [ "$c" = "/" ]; then
			newt="$sel"
		else
			newt="$(realpath "${target}/${sel}")"
		fi

		if [ -e "$newt" ]; then
			target="$newt"
			if [ ! -d "$target" ]; then

				# Got the iso file
				if format=$(dmenu -P -p "Warning Device will be erased | sudo " |\
					sudo -S dd bs=4M\
					if=$target of=/dev/$usb\
					status=progress oflag=sync); then
					notify-send "Bootable USB Created" "$usb"
				else
					notify-send -u critical\
						"Error Creating Bootable USB" "$usb"
				fi
				exit 0

			fi
		fi
	done
fi
