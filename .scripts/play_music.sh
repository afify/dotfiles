#!/usr/bin/env bash

case $1 in
	choose)
		chosen=$(ls /mnt/data/sounds | dmenu -i -p "Choose folder: ")
		if [ $chosen ];then
			pkill vlc
			cvlc /mnt/data/sounds/$chosen -Z
		# 	killall mpv
		# 	mpv --shuffle /mnt/data/sounds/$chosen
		fi ;;
	play-pause)
		case $(uname) in
			Linux)   playerctl play-pause ;;
			OpenBSD) ;;
		esac ;;
	next)
		case $(uname) in
			Linux)   playerctl next ;;
			OpenBSD) ;;
		esac ;;
	previous)
		case $(uname) in
			Linux)    playerctl previous ;;
			OpenBSD)  ;;
		esac ;;
esac
