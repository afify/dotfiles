#!/usr/bin/env sh

######################################################################
# @author      : hassan (hassan@alien)
# @file        : firmware
# @created     : Wednesday Oct 09, 2019 14:07:07 +03
#
# @description : 
######################################################################

git clone https://aur.archlinux.org/aic94xx-firmware.git
cd aic94xx-firmware
makepkg -sri
git clone https://aur.archlinux.org/wd719x-firmware.git
cd wd719x-firmware
makepkg -sri

sudo mkinitcpio -p linux




