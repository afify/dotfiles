#!/usr/bin/env bash

chech_source(){
if [[ ! -d $1 ]]; then
	src=$(df | egrep -o "/mnt/[a-zA-Z0-9]+" |\
		dmenu -i -p "Default Not found Select other source ")
else
	src=$1
fi
}

ask_decrypt(){
	gpg2 --output $output --decrypt $enc_file
}

case $1 in
	data)
		default_des="/tmp"
		default_src="/mnt/4tb/backups/data_backup"
		check_source $default_src
		backup_file=$(ls $src | sort -r | head -n 1)
		tar xzf $backup_file --directory $default_des
		# tar -xzvf archive.tar.gz -C /tmp
		# tar -xjvf archive.tar.bz2 -C /tmp

		;;

	home)
		default_des=$HOME
		home_files=".ssh .gnupg .password-store .scripts"
		default_src="/mnt/sd1/backups"
		if [[ ! -d $default_src ]];
		then default_src="/mnt/4tb/backups/home"; fi
		check_source $default_src
		backup_file=$(ls $_src | sort -r | head -n 1)
		tar --extract --file=$backup_file $home_files --directory $HOME
		tar --extract --file=$backup_file dev --directory /mnt/data
		;;
esac
