#!/usr/bin/env bash

#==============================================================================
# Name        : find_replace_word
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

dir=$1
old_word=$2
new_word=$3

# sed -i -- "s/$old_word/$new_word/g" *
find $dir -type f -exec sed -i "s/$old_word/$new_word/g" {} +
