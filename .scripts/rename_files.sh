#!/usr/bin/env bash

#==============================================================================
# Name        : rename_files
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description :
#==============================================================================

# changed_string="[EgyBest]."
# to=""
# filename_regex=\[EgyBest\].*
# rename $changed_string $to $filename_regex

rename "[EgyBest]." ""  \[EgyBest\].*

