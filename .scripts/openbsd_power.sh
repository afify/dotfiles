#!/usr/bin/env bash

# Since this is a laptop, you'll want to enable power management to save battery life:

rcctl enable apmd
rcctl set apmd flags -A
rcctl start apmd
