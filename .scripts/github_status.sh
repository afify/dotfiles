#!/usr/bin/env bash

#==============================================================================
# Name        : github
# GitHub      : Afify
# Copyright   : MIT
# Version     :
# Description :
#==============================================================================

github_icon=""
pullrequest_icon=""
issue_icon=""
view_icon=""
clone_icon=""
owner=Afify
repos=( "/mnt/data/dev/azan"
		"/mnt/data/dev/splanner"
	)

for i in "${repos[@]}"
do
	repo_name=$(basename $i)
	declare -n repo_clones="${repo_name}_clones"
	declare -n repo_views="${repo_name}_views"
	declare -n repo_result="${repo_name}_result"
	declare -n repo_pr="${repo_name}_pr"
	declare -n repo_issues="${repo_name}_issues"

	repo_clones=$(hub api /repos/$owner/$repo_name/traffic/clones |\
		jq -r '.uniques')
	repo_views=$(hub api /repos/$owner/$repo_name/traffic/views |\
		jq -r '.uniques')

	repo_pr=$(git --git-dir $i/.git pullrequest | wc -l)
	repo_issues=$(git --git-dir $i/.git issue | wc -l)

	repo_result="[$repo_name] $clone_icon $repo_clones $view_icon $repo_views $pullrequest_icon $repo_pr $issue_icon $repo_issues"
done

notify-send -t 20000 "Github Traffic" "$azan_result\n$splanner_result"
