#!/usr/bin/env bash

case $(uname) in
	Linux) st sudo -- bash -c "pacman -Syyu; yaourt -Su --aur; read -p 'press enter to exit'" ;;
	OpenBSD) st doas -- bash -c "syspatch; sysupgrade; fw_update; pkg_add -u; read -p 'press enter to exit'" ;;
esac
