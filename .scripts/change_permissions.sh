#!/usr/bin/env bash

#==============================================================================
# Name        : change_permissions
# GitHub      : Afify
# Copyright   : MIT
# Version     : 0.1
# Description : Change Directory and files permission
#               pass an argument of target dir
#               TODO
#                  Create default dir current dir
#==============================================================================
dir=$1

find $dir -type d -exec chmod 755 {} \;
find $dir -type f -exec chmod 644 {} \;
