#!/usr/bin/env bash

dir=$1
word=$2
# options=-rnw
# grep $options $dir -e $word
case $(uname) in
	Linux) options='--color=always -R' ;;
	OpenBSD) options=-R ;;
esac
grep "$options" "$word" "$dir"
