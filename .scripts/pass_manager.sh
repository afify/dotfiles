#!/usr/bin/env bash
# replacment of passmenu
# TODO update all passwords

# pass init $gpg_id
# pass edit $1
# pass insert $1                      # manual insert password
# pass generate $service_name $length # generate new password

gpg_id=$(cat "$HOME/.password-store/.gpg-id")
path=$HOME/.password-store/
chosen=$(ls $path | sed -r 's/(\.gpg|.gpg-id)//g' | dmenu -i -p "Pass " -l 10)
if [[ $chosen ]]; then pass -c $chosen; fi
