#!/usr/bin/env bash

# Create a C project: git init + 8 files
#    (license),  (README), (Makefile) (config.mk)
#    (man page) (gitignore) (config.def.h) (c file)

proj=$1
Proj=${proj^}
year=$(date +'%Y')
mkdir $proj
cd $proj
git init > /dev/null
git remote add github git@github.com:Afify/$proj.git > /dev/null

# Create License
#==============================================================================
echo "MIT License

Copyright (c) $year Hassan Afify <hassan at afify dot dev>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE." > ./LICENSE

# Create Makefile
#==============================================================================
echo "# $Proj
# See LICENSE file for copyright and license details.

include config.mk

SRC = $proj.c
OBJ = \${SRC:.c=.o}

all: options $proj

options:
	@echo $proj build options:
	@echo \"CFLAGS  = \$(STCFLAGS)\"
	@echo \"LDFLAGS = \$(STLDFLAGS)\"
	@echo \"CC      = \$(CC)\"

config.h:
	cp config.def.h config.h

.c.o:
	\$(CC) \$(STCFLAGS) -c \$<

$proj.o: config.h

\$(OBJ): config.h config.mk

$proj: \$(OBJ)
	\$(CC) -o \$@ \$(OBJ) \$(STLDFLAGS)

clean:
	rm -f \$(OBJ) $proj

install: $proj
	mkdir -p \$(DESTDIR)\$(PREFIX)/bin
	cp -f $proj \$(DESTDIR)\$(PREFIX)/bin
	chmod 755 \$(DESTDIR)\$(PREFIX)/bin/$proj
	mkdir -p \$(DESTDIR)\$(MANPREFIX)/man1
	sed \"s/VERSION/\$(VERSION)/g\" < $proj.1 > \$(DESTDIR)\$(MANPREFIX)/man1/$proj.1
	chmod 644 \$(DESTDIR)\$(MANPREFIX)/man1/$proj.1

uninstall:
	rm -f \$(DESTDIR)\$(PREFIX)/bin/$proj
	rm -f \$(DESTDIR)\$(MANPREFIX)/man1/proj.1

.PHONY: all clean install uninstall"> ./Makefile

# Create config.mk
#==============================================================================
echo "# $proj version
VERSION = 0.1

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = \$(PREFIX)/share/man

#includes and libs
STD = -std=c99
WARN = -pedantic -Wall -Wno-deprecated-declarations -Os
# WARN = -bench -Wall -Werror -Wunusupported -Wwrite-strings
LIBS = -lm

# flags
STCFLAGS = \$(STD) \$(WARN) -DVERSION=\\\"\${VERSION}\\\" -D_POSIX_C_SOURCE=200809L
STLDFLAGS= \$(LIBS) \$(LDFLAGS)

# compiler and linker
CC = cc
# CC = tcc"> ./config.mk

# Create gitignore
#==============================================================================
echo "$proj
*.db
*.txt
*.log
.o" > ./.gitignore

# Create Readme
#==============================================================================
echo "$Proj
====
$proj is a simple {description}

Installation
------------
\`\`\`sh
$ git clone https://github.com/Afify/$proj.git
$ cd $proj/
$ make
$ sudo make install
\`\`\`

Run
---
\`\`\`sh
\$ $proj
\`\`\`

Options
-------
\`\`\`sh
\$ $proj [-vah]
\$ man $proj
\`\`\`

Configuration
-------------
The configuration of $proj is done by creating a custom config.h
and (re)compiling the source code.

Philosophy, Contribution & Code Style
-------------------------------------
- [Contribution].
- <img src=\"https://suckless.org/logo.svg\" width=\"13\"\/\> [Philosophy].
- <img src=\"https://suckless.org/logo.svg\" width=\"13\"\/\> [Code Style].

Ask Questions
-------------
You are welcome to submit questions and bug reports as Github Issues.

Copyright and License
---------------------
$Proj is provided under the MIT license.

[Contribution]: <https://github.com/Afify/$proj/tree/master/CONTRIBUTING.md>
[Philosophy]: <http://suckless.org/philosophy/>
[Code Style]: <http://suckless.org/coding_style/>" > ./README.md

# Create man page
#==============================================================================
echo ".TH $Proj 1 $proj\-VERSION
.SH NAME
$proj \- simple
.SH SYNOPSIS
.B $proj
.RB [ \-vah ]
.SH DESCRIPTION
$proj is a simple
.P
.SH OPTIONS
.TP
.B \-v
prints version information to standard output, then exits.
.TP
.B \-h
prints usage help.
.SH USAGE
.TP
.B $proj
.SH CUSTOMIZATION
$proj is customized by creating a custom config.h and (re)compiling the source
code. This keeps it fast, secure and simple.
.SH AUTHORS
https://github.com/Afify
.SH ISSUES
https://github.com/Afify/$proj/issues"> ./$proj.1

# Create config.def
#==============================================================================
echo "/* See LICENSE file for copyright and license details.*/

#ifndef CONFIG_H
#define CONFIG_H



#endif /* CONFIG_H */"> ./config.def.h

# Create c file
#==============================================================================
echo "/* See LICENSE file for copyright and license details. */

/* macros */
/* typedef */
/* function declarations */
/* function implementations */

int
main(void)
{

	return 0;
}"> ./$proj.c
