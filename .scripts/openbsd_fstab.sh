#!/usr/bin/env bash

You can greatly improve disk performance by enabling softupdates and the noatime option for all your local partitions in /etc/fstab. Add the softdep,noatime options to each partition (except swap) as I've demonstrated below. Don't copy and paste this directly, as your disk identifier will be different.

/etc/fstab
0364c44477d30004.b none swap sw
0364c44477d30004.a / ffs rw,softdep,noatime 1 1
0364c44477d30004.l /home ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.d /tmp ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.f /usr ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.g /usr/X11R6 ffs rw,softdep,noatime,nodev 1 2
0364c44477d30004.h /usr/local ffs rw,softdep,noatime,wxallowed,nodev 1 2
0364c44477d30004.k /usr/obj ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.j /usr/src ffs rw,softdep,noatime,nodev,nosuid 1 2
0364c44477d30004.e /var ffs rw,softdep,noatime,nodev,nosuid 1 2

