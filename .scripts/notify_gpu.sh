#!/usr/bin/env bash

ps=$(nvidia-smi pmon -c 1 | tail -n +3 |awk '{print $2 " " $4 "% " $8}' | column -t)
notify-send "GPU" "$ps"
