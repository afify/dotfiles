#!/usr/bin/env sh

user=$(whoami)
ostype=$(uname)

case $ostype in
	Linux)   auth=sudo ;;
	OpenBSD) auth=doas ;;
esac

cd /mnt/
$auth mkdir 4tb 1tb sd1 sd2 usb1 usb2 phone1 phone2 temp1 temp2
$auth chown -R $user /mnt/*
