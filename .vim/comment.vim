" Commment one line (Controle /)
autocmd FileType vim    nnoremap <buffer> <C-_> <Home>i" <esc>
autocmd FileType python nnoremap <buffer> <C-_> <Home>i# <esc>
autocmd FileType conf   nnoremap <buffer> <C-_> <Home>i# <esc>
autocmd FileType dosini nnoremap <buffer> <C-_> <Home>i# <esc>
autocmd FileType sh     nnoremap <buffer> <C-_> <Home>i# <esc>
autocmd FileType make   nnoremap <buffer> <C-_> <Home>i# <esc>
autocmd FileType c      nnoremap <buffer> <C-_> <Home>i// <esc>
autocmd FileType c      nnoremap <buffer> <Leader>/ <Home>i/* <End> */<esc>
autocmd FileType cpp    nnoremap <buffer> <C-_> <Home>i// <esc>
autocmd FileType html   nnoremap <buffer> <C-_> <Home>i<!-- <End> --><esc>
autocmd FileType css    nnoremap <buffer> <C-_> <Home>i/* <End> */<esc>

" Uncomment one line (Shift /)
autocmd FileType vim    nnoremap <buffer> ? :s/^" /<CR>
autocmd FileType python nnoremap <buffer> ? :s/^# /<CR>
autocmd FileType conf   nnoremap <buffer> ? :s/^# /<CR>
autocmd FileType dosini nnoremap <buffer> ? :s/^# /<CR>
autocmd FileType sh     nnoremap <buffer> ? :s/^# /<CR>
autocmd FileType make   nnoremap <buffer> ? :s/^# /<CR>
autocmd FileType c      nnoremap <buffer> ? :s/^\/\/ /<CR>
autocmd FileType c      nnoremap <buffer> <Leader>/ :s/^/* /<CR>:s/ */$/<CR>
autocmd FileType cpp    nnoremap <buffer> ? :s/^\/\/ /<CR>
autocmd FileType html   nnoremap <buffer> ? :s/^<!-- /<CR>:s/ -->$/<CR>
autocmd FileType css    nnoremap <buffer> ? :s/^/* /<CR>:s/ */$/<CR>

" Commment selected lines (Controle /)
autocmd FileType vim    vnoremap <buffer> <C-_> <Home><C-v>I" <esc><esc>
autocmd FileType python vnoremap <buffer> <C-_> <Home><C-v>I# <esc><esc>
autocmd FileType conf   vnoremap <buffer> <C-_> <Home><C-v>I# <esc><esc>
autocmd FileType dosini vnoremap <buffer> <C-_> <Home><C-v>I# <esc><esc>
autocmd FileType sh     vnoremap <buffer> <C-_> <Home><C-v>I# <esc><esc>
autocmd FileType make   vnoremap <buffer> <C-_> <Home><C-v>I# <esc><esc>
autocmd FileType c      vnoremap <buffer> <C-_> <Home><C-v>I// <esc><esc>
autocmd FileType cpp    vnoremap <buffer> <C-_> <Home><C-v>I// <esc><esc>

" Uncomment selected lines (Shift /)
autocmd FileType vim    vnoremap <buffer> ? :s/^" /<CR>
autocmd FileType python vnoremap <buffer> ? :s/^# /<CR>
autocmd FileType conf   vnoremap <buffer> ? :s/^# /<CR>
autocmd FileType dosini vnoremap <buffer> ? :s/^# /<CR>
autocmd FileType sh     vnoremap <buffer> ? :s/^# /<CR>
autocmd FileType make   vnoremap <buffer> ? :s/^# /<CR>
autocmd FileType c      vnoremap <buffer> ? :s/^\/\/ /<CR>
autocmd FileType cpp    vnoremap <buffer> ? :s/^\/\/ /<CR>
