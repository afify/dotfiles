"==============================================================================
" Main
"==============================================================================
set nocompatible
let mapleader=" "
set shell=bash
set encoding=utf-8
set fileformat=unix
set noswapfile
set backspace=indent,eol,start

"==============================================================================
" Plugins
"==============================================================================
source ~/.vim/comment.vim

call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter'
" Plug 'justinmk/vim-syntax-extra'
" Plug 'ap/vim-css-color'
" Plug 'mechatroner/rainbow_csv'
Plug 'MTDL9/vim-log-highlighting'
Plug 'maralla/completor.vim'
Plug 'vim-syntastic/syntastic'
call plug#end()

"==============================================================================
" Display
"==============================================================================
colorscheme minimal
set list
set listchars=tab:>—,nbsp:␣,trail:.
set tabstop=4
set shiftwidth=4
set noexpandtab
set splitbelow splitright
set wildmenu
set wildmode=list:full,full
set ignorecase "search case insensitive
set smartcase "if uppercase case sensitive

autocmd Filetype python setlocal ts=4 sw=4 expandtab
autocmd FileType c      setlocal ts=8 sw=8
autocmd FilterWritePre * if &diff | set foldcolumn=0 | endif

" set noautoindent
" set linebreak
" filetype plugin indent on

"==============================================================================
" Keymaping
"==============================================================================
"copy to global clipboard
vmap <C-c> y:call system("xclip -i -selection clipboard", getreg("\""))<cr>:call system("xclip -i", getreg("\""))<cr>

" replace word with yanked
nnoremap <C-p> cw<C-r>0<ESC>

" paste
nnoremap <C-c> :set invpaste paste?<CR>
set pastetoggle=<C-c>
set showmode

"Move lines up and down with controle + k or j
nnoremap <silent> <C-j> :move +1 <CR>
nnoremap <silent> <C-k> :move -2 <CR>

"Disable keys in Normal mode
no <Up>        <Nop>
no <Down>      <Nop>
no <Left>      <Nop>
no <Right>     <Nop>
no <Space>     <Nop>
no <PageUp>    <Nop>
no <PageDown>  <Nop>
no <BackSpace> <Nop>
no <F1>        <Nop>
no q           <Nop>
no Q           <Nop>

"Disable keys in Insert mode
ino <Up>       <Nop>
ino <Down>     <Nop>
ino <PageUp>   <Nop>
ino <PageDown> <Nop>
ino <End>      <Nop>
ino <Home>     <Nop>
ino <F1>       <Nop>

" Add new line
noremap J o<esc>k
noremap K O<esc>j

" Go (start|end) of line
noremap H ^
noremap L $

" Split
nnoremap <Leader><CR> <Esc>:vsplit

nnoremap <silent> <Leader>= :vertical resize +1<CR>
nnoremap <silent> <Leader>- :vertical resize -1<CR>
nnoremap <silent> <Leader>0 :vertical resize 87<CR>

nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l
nnoremap <Leader>h <C-w>h

" Move the split
nnoremap <Leader>L <C-w>L
nnoremap <Leader>H <C-w>H
nnoremap <Leader>J <C-w>J
nnoremap <Leader>K <C-w>K

" Search
nnoremap <Leader>f :set hlsearch<CR>/^[a-z].*(.*)$<CR>
nnoremap <Leader>o :set hlsearch!<CR>/\%80v.\+/<CR>
nnoremap <Leader>s :set hlsearch!<CR>
nnoremap * *zz
nmap n nzz

" Auto close
" inoremap ( ()<Left>
" inoremap { {}<Left>
" inoremap [ []<Left>
" inoremap " ""<Left>
" inoremap ' ''<Left>

" Quit
nnoremap <Leader>q :qa!<CR>

" Fold
nnoremap za :set foldmethod=syntax<CR>za
nnoremap <Leader>z :set foldmethod=syntax<CR>zM
nnoremap <Leader>Z :set foldmethod=syntax<CR>zR

" Syntastic
nnoremap <Leader>a :SyntasticCheck<CR>
nnoremap <Leader>n :lnext<CR>

" Custom functions
nnoremap <Leader>c :call Clean_file()<CR><CR>

nnoremap <Leader>r :set rightleft<CR>
nnoremap <Leader>R :set norightleft<CR>

" surround word
nnoremap <Leader>' i"<ESC>ea"<ESC><Right>
nnoremap <Leader>" i"<ESC>A"<ESC><Right>
nnoremap <Leader>[ i{<ESC>ea"<ESC><Right>

" Enter command mode
noremap ; :

"==============================================================================
" Status Line
"==============================================================================
set laststatus=2
set statusline=\ %F%#StatusLine_g#\ %m%#StatusLine_r#\ %r%*%=
set statusline+=%y\ %{&encoding}\ %{&fileformat}

" Warnings
set statusline+=%#StatusLine_o#
set statusline+=%{StatuslineTabWarning()}
set statusline+=%{StatuslineTrailingSpaceWarning()}
set statusline+=%*

"recalculate the tab warning flag when idle and after writing
autocmd cursorhold,bufwritepost * unlet! b:statusline_tab_warning
autocmd cursorhold,bufwritepost * unlet! b:statusline_trailing_space_warning

function! StatuslineTrailingSpaceWarning()
	if !exists("b:statusline_trailing_space_warning")
		let result = search('\s\+$', 'nw') != 0
		if result
			let b:statusline_trailing_space_warning = ' [TS]'
		else
			let b:statusline_trailing_space_warning = ''
		endif
	endif
	return b:statusline_trailing_space_warning
endfunction

function! StatuslineTabWarning()
	if !exists("b:statusline_tab_warning")
		let b:statusline_tab_warning = ''
		if !&modifiable
			return b:statusline_tab_warning
		endif
		let tabs = search('^\t', 'nw') != 0
		"find spaces that arent used as alignment in the first indent column
		let spaces = search('^ \{' . &ts . ',}[^\t]', 'nw') != 0
		if tabs && spaces
			let b:statusline_tab_warning =  ' [MI]'
		elseif (spaces && !&et) || (tabs && &et)
			let b:statusline_tab_warning = '[&et]'
		endif
	endif
	return b:statusline_tab_warning
endfunction

"==============================================================================
" Linting
"==============================================================================
let g:syntastic_python_checkers = ['pylint2']
let g:syntastic_sh_checkers     = ["shellcheck"]
let g:syntastic_c_checkers      = ["splint"]
" Moderately strict checking
let g:syntastic_c_splint_args   = "-checks -DVERSION=\\\"VERSION\\\""

set statusline+=%#statusline_r#
set statusline+=\ %{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_check_on_wq              = 0
let g:syntastic_auto_loc_list            = 0
let g:syntastic_check_on_open            = 1
let g:syntastic_enable_highlighting      = 0 "underline word
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_jump                = 0

" Symbols icon
let g:syntastic_error_symbol         = ''
let g:syntastic_warning_symbol       = ''
let g:syntastic_style_error_symbol   = ''
let g:syntastic_style_warning_symbol = '!'

"==============================================================================
" Folding
"==============================================================================
" set nofen
set foldnestmax=1

function! MyFoldText() " {{{
let line = getline(v:foldstart)
let nucolwidth = &fdc + &number * &numberwidth
let windowwidth = winwidth(0) - nucolwidth - 3
let foldedlinecount = v:foldend - v:foldstart
let onetab = strpart('          ', 0, &tabstop)
let line = substitute(line, '\t', onetab, 'g')
let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
let fillcharcount = 81 - len(line) - len(foldedlinecount)
let rest  = windowwidth - fillcharcount
return line . repeat(" ",fillcharcount) . foldedlinecount .' ' . repeat(" ", rest)
endfunction " }}}

set foldtext=MyFoldText()

"==============================================================================
" File type highlight
"==============================================================================
setf dosini
augroup filetypedetect
	autocmd BufNewFile,BufRead *.h     set filetype=c
	autocmd BufNewFile,BufRead *.csv   set filetype=csv
	autocmd BufNewFile,BufRead *.gpg   set filetype=gpg
	autocmd BufNewFile,BufRead LICENSE set filetype=text
	autocmd BufNewFile,BufRead README  set filetype=markdown
augroup END

" csv colums color
let g:rcsv_colorpairs = [
	\[124, 'red'],
	\[186, 'green'],
	\[172, 'yellow'],
	\[66, 'blue'],
	\[132, 'purple'],
	\[72, 'aqua'],
	\[166, 'orange'],
	\['darkgreen', 'darkgreen'],
	\['darkmagenta', 'darkmagenta'],
	\['darkcyan', 'darkcyan']]

"==============================================================================
" Auto Completor
"==============================================================================
let g:completor_auto_trigger = 1
let g:completor_clang_binary = '/usr/bin/clang'
let g:completor_complete_options = 'menuone,noselect,preview'

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"==============================================================================
" Remove characters function
"==============================================================================
function! Clean_file()
	" Save cursor line
	let save_pos = getpos(".")
	" Remove spaces from begin & end of line
	" Remove spaces after tab
	" Remove tab\n
	" Remove file last line if empty
	" Remove more than 2 empty lines
	%s/ \+$//e |
	\%s/^ \+//e |
	\%s/\t \+/\t/e |
	\%s/\t\+\n//e |
	\%s/\($\n\)\+\%$//e |
	\%s/^\n\{2,\}//e
	" Return to saved cursor
	call setpos(".", save_pos)
endfunction

"==============================================================================
" File Templates
"==============================================================================
let g:filename      = expand("%:r")
let g:filename_ext  = expand("%:e")
let g:upperfilename = toupper(filename)

autocmd BufNewFile *.sh  exe "normal!ggi#!/usr/bin/env bash"
autocmd BufNewFile *.py  exe "normal!ggi#!/usr/bin/env python3"
autocmd BufNewFile *.h  :call TempH()
autocmd BufNewFile *.c  :call TempC()

function! TempH()
	exe "normal!ggi/* See LICENSE file for copyright and license details. */"
	exe "normal!o#ifndef ".g:upperfilename."_H"
	exe "normal!o#define ".g:upperfilename."_H"
	exe "normal!o#endif /* ".g:upperfilename."_H */"
	exe "normal!kk"
endfunction

function! TempC()
	exe "normal!ggi/* See LICENSE file for copyright and license details. */"
	exe "normal!o/* macros */"
	exe "normal!o/* typedef */"
	exe "normal!o/* function declarations */"
	exe "normal!o/* function implementations */"
	exe "normal!ointmain(void){return 0;}"
	exe "normal!kk"
endfunction
